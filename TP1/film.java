public class Film extends Media {
    // Données d'instance spécifiques à la classe Film
    private String realisateur;
    private int annee;

    // Constructeur par défaut (Question 8)
    public Film() {
        super();
        this.realisateur = "";
        this.annee = 0;
    }

    // Constructeur par initialisation (Question 8)
    public Film(String titre, StringBuffer cote, int note, String realisateur, int annee) {
        super(titre, cote, note);
        this.realisateur = realisateur;
        this.annee = annee;
    }

    // Constructeur par copie (Question 8)
    public Film(Film autreFilm) {
        super(autreFilm);
        this.realisateur = autreFilm.realisateur;
        this.annee = autreFilm.annee;
    }

    // Getters et Setters pour les attributs spécifiques de Film (Question 8)
    public String getRealisateur() {
        return realisateur;
    }

    public void setRealisateur(String realisateur) {
        this.realisateur = realisateur;
    }

    public int getAnnee() {
        return annee;
    }

    public void setAnnee(int annee) {
        this.annee = annee;
    }

    // Masquage de la méthode toString (Question 8)
    @Override
    public String toString() {
        return "Film{" +
               "titre='" + getTitre() + '\'' +
               ", cote=" + getCote() +
               ", note=" + getNote() +
               ", realisateur='" + realisateur + '\'' +
               ", annee=" + annee +
               '}';
    }

    // Masquage de la méthode equals (Question 8)
    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        if (!super.equals(obj)) return false;
        Film film = (Film) obj;
        return annee == film.annee && realisateur.equals(film.realisateur);
    }

    // Masquage de la méthode clone (Question 8)
    @Override
    protected Object clone() throws CloneNotSupportedException {
        return new Film(getTitre(), getCote(), getNote(), realisateur, annee);
    }
}
