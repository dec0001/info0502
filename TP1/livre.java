public class Livre extends Media {
    // Données d'instance spécifiques à la classe Livre
    private String auteur;  
    private String isbn;    

    // Constructeur par initialisation (Question 4)
    public Livre(String titre, StringBuffer cote, int note, String auteur, String isbn) {
        super(titre, cote, note);
        this.auteur = auteur;
        this.isbn = isbn;
    }

    // Getter pour l'attribut auteur (Question 4)
    public String getAuteur() {
        return auteur;
    }

    // Setter pour l'attribut auteur (Question 4)
    public void setAuteur(String auteur) {
        this.auteur = auteur;
    }

    // Getter pour l'attribut ISBN (Question 4)
    public String getIsbn() {
        return isbn;
    }

    // Setter pour l'attribut ISBN (Question 4)
    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    // Masquage de la méthode toString (Question 4)
    @Override
    public String toString() {
        return "Livre{" +
               "titre='" + getTitre() + '\'' +
               ", cote=" + getCote() +
               ", note=" + getNote() +
               ", auteur='" + auteur + '\'' +
               ", isbn='" + isbn + '\'' +
               '}';
    }

    // Masquage de la méthode equals (Question 4)
    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        if (!super.equals(obj)) return false;  // Comparaison des attributs hérités de Media
        Livre livre = (Livre) obj;
        return auteur.equals(livre.auteur) && isbn.equals(livre.isbn);
    }

    // Masquage de la méthode clone (Question 4)
    @Override
    protected Object clone() throws CloneNotSupportedException {
        return new Livre(getTitre(), getCote(), getNote(), auteur, isbn);
    }
}
