package edu.info0502.client;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;
import edu.info0502.common.Card;
import edu.info0502.common.PokerPlayer;
import org.eclipse.paho.client.mqttv3.*;

import java.lang.reflect.Type;
import java.util.List;
import java.util.Scanner;

public class PokerClient {
    private static final String MQTT_BROKER = "tcp://localhost:1883";
    private static final String COMMAND_CHANNEL = "poker/commands";
    private static final String RESPONSE_CHANNEL = "poker/responses";

    private String clientId;
    private MqttClient mqttClient;
    private Gson jsonParser;
    private String tableIdentifier;
    private String targetTableId;
    private PokerPlayer player;
    private String playerState;
    private volatile String activePlayerId;
    private volatile String activePlayerName;
    private volatile boolean gameReady = false;

    public PokerClient(String clientId) {
        this.clientId = clientId.trim();
        this.jsonParser = new Gson();
        this.playerState = "Waiting";
        this.tableIdentifier = null;
        this.targetTableId = null;
        this.activePlayerId = "";
        this.activePlayerName = "";

        try {
            mqttClient = new MqttClient(MQTT_BROKER, clientId);
            MqttConnectOptions options = new MqttConnectOptions();
            options.setCleanSession(true);
            mqttClient.connect(options);

            mqttClient.subscribe(RESPONSE_CHANNEL, this::handleIncomingMessage);
            System.out.println("PokerClient connecté au serveur et abonné au canal : " + RESPONSE_CHANNEL);
        } catch (MqttException e) {
            System.out.println("Erreur de connexion au serveur MQTT :");
            e.printStackTrace();
        }
    }

    private void handleIncomingMessage(String topic, MqttMessage message) throws Exception {
        String messagePayload = new String(message.getPayload());
        System.out.println("\n--- Réponse reçue ---");
        System.out.println(messagePayload);

        JsonObject jsonMessage = JsonParser.parseString(messagePayload).getAsJsonObject();
        String responseType = jsonMessage.get("response").getAsString();

        switch (responseType) {
            case "CREATE_TABLE":
                processCreateTableResponse(jsonMessage);
                break;
            case "JOIN_TABLE":
                processJoinTableResponse(jsonMessage);
                break;
            case "GAME_STATE":
                processGameState(jsonMessage);
                break;
            case "CLOSE_TABLE":
                processCloseTableResponse(jsonMessage);
                break;
            case "GAME_OVER":
                processGameOverResponse(jsonMessage);
                break;
            default:
                System.out.println("Réponse inconnue : " + responseType);
        }
    }

    private void processCreateTableResponse(JsonObject jsonResponse) {
        boolean success = jsonResponse.get("success").getAsBoolean();
        if (success) {
            tableIdentifier = jsonResponse.get("tableId").getAsString().trim();
            playerState = "InGame";
            System.out.println("Table créée avec succès. ID de la table : " + tableIdentifier);
        } else {
            System.out.println("Échec de la création de la table.");
        }
    }

    private void processJoinTableResponse(JsonObject jsonResponse) {
        boolean success = jsonResponse.get("success").getAsBoolean();
        if (success) {
            tableIdentifier = targetTableId;
            playerState = "InGame";
            System.out.println("Rejoint la table avec succès. ID de la table : " + tableIdentifier);
        } else {
            System.out.println("Échec de la jonction à la table.");
        }
    }

    private void processGameState(JsonObject jsonResponse) {
        try {
            String receivedTableId = jsonResponse.get("tableId").getAsString().trim();

            if (this.tableIdentifier == null && receivedTableId.equalsIgnoreCase(targetTableId)) {
                this.tableIdentifier = receivedTableId;
            }

            if (!receivedTableId.equalsIgnoreCase(this.tableIdentifier)) {
                return;
            }

            Type playerListType = new TypeToken<List<PokerPlayer>>() {}.getType();
            List<PokerPlayer> players = jsonParser.fromJson(jsonResponse.get("players"), playerListType);

            Type cardListType = new TypeToken<List<Card>>() {}.getType();
            List<Card> communityCards = jsonParser.fromJson(jsonResponse.get("communityCards"), cardListType);

            boolean gameStarted = jsonResponse.get("gameStarted").getAsBoolean();
            activePlayerId = jsonResponse.has("currentPlayerId") ? jsonResponse.get("currentPlayerId").getAsString().trim() : "";
            activePlayerName = jsonResponse.has("currentPlayerName") ? jsonResponse.get("currentPlayerName").getAsString().trim() : "";

            if (player != null) {
                for (PokerPlayer p : players) {
                    if (p.getUniqueId().equalsIgnoreCase(player.getUniqueId())) {
                        player.setChipCount(p.getChipCount());
                        player.setHasFolded(p.isHasFolded());
                        player.setHand(p.getHand());
                        break;
                    }
                }
            }

            System.out.println("Table : " + receivedTableId);
            System.out.println("Phase : " + jsonResponse.get("phase").getAsString());
            System.out.println("Joueurs :");
            for (PokerPlayer p : players) {
                String state = p.isHasFolded() ? "Fold" : "Active";
                String indicator = p.getUniqueId().equalsIgnoreCase(activePlayerId) ? " <-- Turn" : "";
                System.out.println("- " + p.getName() + " | Chips: " + p.getChipCount() + " | State: " + state + indicator);
            }

            System.out.println("Cartes Communes :");
            for (Card card : communityCards) {
                System.out.println(card);
            }

            gameReady = true;
        } catch (Exception e) {
            System.out.println("Erreur lors du traitement de GAME_STATE : " + e.getMessage());
        }
    }

    private void processCloseTableResponse(JsonObject jsonResponse) {
        boolean success = jsonResponse.get("success").getAsBoolean();
        if (success) {
            System.out.println("Table fermée avec succès.");
            tableIdentifier = null;
            playerState = "Waiting";
        } else {
            System.out.println("Échec de la fermeture de la table.");
        }
    }

    private void processGameOverResponse(JsonObject jsonResponse) {
        String winnerName = jsonResponse.get("winnerName").getAsString();
        String winningCombination = jsonResponse.get("winningCombination").getAsString();
        int potValue = jsonResponse.get("pot").getAsInt();

        System.out.println("Fin de la partie. Gagnant : " + winnerName + " avec " + winningCombination + ". Pot : " + potValue + " chips.");
    }

    public void createTable() {
        JsonObject command = new JsonObject();
        command.addProperty("command", "CREATE_TABLE");
        sendCommand(command);
    }

    public void joinTable(String tableId, String playerName, int chips) {
        this.targetTableId = tableId.trim();
        this.player = new PokerPlayer(clientId, playerName, chips);
        JsonObject command = new JsonObject();
        command.addProperty("command", "JOIN_TABLE");
        command.addProperty("tableId", tableId.trim());
        command.addProperty("playerId", player.getUniqueId());
        command.addProperty("playerName", player.getName());
        command.addProperty("chips", player.getChipCount());
        sendCommand(command);
    }

    public void closeTable(String tableId) {
        JsonObject command = new JsonObject();
        command.addProperty("command", "CLOSE_TABLE");
        command.addProperty("tableId", tableId.trim());
        sendCommand(command);
    }

    public void sendAction(String action, int amount) {
        JsonObject command = new JsonObject();
        command.addProperty("command", "PLAYER_ACTION");
        command.addProperty("tableId", tableIdentifier);
        command.addProperty("playerId", clientId);
        command.addProperty("action", action);
        if (action.equalsIgnoreCase("bet") || action.equalsIgnoreCase("raise")) {
            command.addProperty("amount", amount);
        }
        sendCommand(command);
    }

    private void sendCommand(JsonObject command) {
        try {
            String payload = jsonParser.toJson(command);
            MqttMessage message = new MqttMessage(payload.getBytes());
            message.setQos(1);
            mqttClient.publish(COMMAND_CHANNEL, message);
            System.out.println("Commande publiée : " + payload);
        } catch (MqttException e) {
            System.out.println("Erreur lors de l'envoi de la commande :");
            e.printStackTrace();
        }
    }
}
