package edu.info0502.common;

import java.io.Serializable;

/**
 * Classe représentant un participant dans le jeu de poker.
 * Cette classe est sérialisable pour permettre sa transmission via le réseau.
 */
public class PokerPlayer implements Serializable {
    private static final long serialVersionUID = 1L;

    private String uniqueId; // Identifiant unique du joueur
    private String name; // Nom du joueur
    private PokerHand hand; // Main de poker actuelle du joueur
    private boolean hasFolded; // Indique si le joueur s'est couché
    private int chipCount; // Nombre de jetons détenus

    /**
     * Constructeur par défaut (nécessaire pour certaines opérations comme la désérialisation).
     */
    public PokerPlayer() {}

    /**
     * Constructeur avec le nom et les jetons initiaux.
     *
     * @param name      Le nom du joueur.
     * @param chipCount Le nombre initial de jetons.
     */
    public PokerPlayer(String name, int chipCount) {
        this.name = name;
        this.hasFolded = false;
        this.chipCount = chipCount;
    }

    /**
     * Constructeur complet avec un identifiant unique, un nom et des jetons.
     *
     * @param uniqueId  Identifiant unique du joueur.
     * @param name      Nom du joueur.
     * @param chipCount Nombre initial de jetons.
     */
    public PokerPlayer(String uniqueId, String name, int chipCount) {
        this.uniqueId = uniqueId;
        this.name = name;
        this.hasFolded = false;
        this.chipCount = chipCount;
    }

    // Getters et Setters

    public String getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(String uniqueId) {
        this.uniqueId = uniqueId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public PokerHand getHand() {
        return hand;
    }

    public void setHand(PokerHand hand) {
        this.hand = hand;
    }

    public boolean isHasFolded() {
        return hasFolded;
    }

    public void setHasFolded(boolean hasFolded) {
        this.hasFolded = hasFolded;
    }

    public int getChipCount() {
        return chipCount;
    }

    public void setChipCount(int chipCount) {
        this.chipCount = chipCount;
    }

    /**
     * Ajoute un montant de jetons au joueur.
     *
     * @param amount Montant à ajouter.
     */
    public void addChips(int amount) {
        this.chipCount += amount;
    }

    /**
     * Retire un montant de jetons du joueur, si possible.
     *
     * @param amount Montant à retirer.
     * @return true si le retrait a réussi, false sinon.
     */
    public boolean removeChips(int amount) {
        if (this.chipCount >= amount) {
            this.chipCount -= amount;
            return true;
        }
        return false;
    }

    /**
     * Représente le joueur sous forme de chaîne.
     *
     * @return Le nom du joueur.
     */
    @Override
    public String toString() {
        return name;
    }

    /**
     * Vérifie si deux joueurs sont égaux en comparant leurs identifiants uniques.
     *
     * @param obj L'objet à comparer.
     * @return true si les joueurs ont le même identifiant, false sinon.
     */
    @Override
    public boolean equals(Object obj) {
        if (obj instanceof PokerPlayer) {
            PokerPlayer other = (PokerPlayer) obj;
            return uniqueId.equals(other.uniqueId);
        }
        return false;
    }

    /**
     * Retourne le code de hachage basé sur l'identifiant unique du joueur.
     *
     * @return Le code de hachage.
     */
    @Override
    public int hashCode() {
        return uniqueId.hashCode();
    }
}
