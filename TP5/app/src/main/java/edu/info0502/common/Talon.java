package edu.info0502.common;

import java.util.*;

public class CardShoe {
    private List<Card> shoe;
    private int version; 
    CardShoe(int version) {
        this.version = version;
        this.shoe = new ArrayList<>();
        initializeShoe();
        shuffleShoe();
    }

    private void initializeShoe() {
        String[] ranks = {"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K", "A"};
        String[] suits = {"HEARTS", "DIAMONDS", "CLUBS", "SPADES"};
        for (String rank : ranks) {
            for (String suit : suits) {
                shoe.add(new Card(rank, suit));
            }
        }
    }

    private void shuffleShoe() {
        Collections.shuffle(shoe);
    }

    public Card drawCard() {
        if (shoe.isEmpty()) {
            throw new NoSuchElementException("Le talon est vide !");
        }
        return shoe.remove(shoe.size() - 1);
    }

    public int getRemainingCards() {
        return shoe.size();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (Card card : shoe) {
            sb.append(card.toString()).append("\n");
        }
        return sb.toString();
    }
}
