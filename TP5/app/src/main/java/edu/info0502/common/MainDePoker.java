package edu.info0502.common;

import java.io.Serializable;
import java.util.*;

/**
 * Classe représentant une main de cartes dans un jeu de poker.
 * Cette classe est sérialisable pour permettre la transmission via le réseau.
 */
public class PokerHand implements Comparable<PokerHand>, Serializable {
    private static final long serialVersionUID = 1L;

    private List<Card> cards;

    /**
     * Enumération des différentes combinaisons possibles dans une main de poker.
     */
    public enum Combination {
        HIGH_CARD(1), PAIR(2), TWO_PAIR(3), THREE_OF_A_KIND(4), STRAIGHT(5),
        FLUSH(6), FULL_HOUSE(7), FOUR_OF_A_KIND(8), STRAIGHT_FLUSH(9), ROYAL_FLUSH(10);

        private final int strength;

        Combination(int strength) {
            this.strength = strength;
        }

        public int getStrength() {
            return strength;
        }
    }

    private Combination combination;
    private List<Integer> cardValues;

    /**
     * Constructeur par défaut (nécessaire pour certaines opérations comme Gson).
     */
    public PokerHand() {}

    /**
     * Constructeur prenant une liste de cartes.
     *
     * @param cards La liste des cartes formant la main.
     */
    public PokerHand(List<Card> cards) {
        this.cards = new ArrayList<>(cards);
        evaluateHand();
    }

    // Getters et Setters

    public List<Card> getCards() {
        return cards;
    }

    public void setCards(List<Card> cards) {
        this.cards = cards;
        evaluateHand();
    }

    public Combination getCombination() {
        return combination;
    }

    public void setCombination(Combination combination) {
        this.combination = combination;
    }

    /**
     * Évalue la main pour déterminer sa combinaison et préparer les valeurs pour comparaison.
     */
    private void evaluateHand() {
        if (cards == null || cards.isEmpty()) {
            return;
        }

        Collections.sort(cards, Collections.reverseOrder());
        cardValues = new ArrayList<>();
        Map<Integer, Integer> valueCounts = new HashMap<>();
        Map<Card.Suit, Integer> suitCounts = new HashMap<>();

        for (Card card : cards) {
            int value = card.getRank().getValue();
            cardValues.add(value);

            valueCounts.put(value, valueCounts.getOrDefault(value, 0) + 1);
            suitCounts.put(card.getSuit(), suitCounts.getOrDefault(card.getSuit(), 0) + 1);
        }

        boolean isStraight = checkStraight();
        boolean isFlush = suitCounts.containsValue(5);

        if (isStraight && isFlush) {
            if (cardValues.get(0) == 14) {
                combination = Combination.ROYAL_FLUSH;
            } else {
                combination = Combination.STRAIGHT_FLUSH;
            }
        } else if (valueCounts.containsValue(4)) {
            combination = Combination.FOUR_OF_A_KIND;
        } else if (valueCounts.containsValue(3) && valueCounts.containsValue(2)) {
            combination = Combination.FULL_HOUSE;
        } else if (isFlush) {
            combination = Combination.FLUSH;
        } else if (isStraight) {
            combination = Combination.STRAIGHT;
        } else if (valueCounts.containsValue(3)) {
            combination = Combination.THREE_OF_A_KIND;
        } else if (Collections.frequency(valueCounts.values(), 2) == 2) {
            combination = Combination.TWO_PAIR;
        } else if (valueCounts.containsValue(2)) {
            combination = Combination.PAIR;
        } else {
            combination = Combination.HIGH_CARD;
        }

        cardValues = prepareComparisonList(valueCounts);
    }

    /**
     * Vérifie si la main constitue une suite (Straight).
     *
     * @return true si la main est une suite, false sinon.
     */
    private boolean checkStraight() {
        if (cards.size() < 5) {
            return false;
        }

        List<Integer> uniqueValues = new ArrayList<>(new HashSet<>(cardValues));
        Collections.sort(uniqueValues, Collections.reverseOrder());

        if (uniqueValues.equals(Arrays.asList(14, 5, 4, 3, 2))) {
            return true;
        }

        for (int i = 0; i <= uniqueValues.size() - 5; i++) {
            boolean isStraight = true;
            for (int j = 1; j < 5; j++) {
                if (!uniqueValues.contains(uniqueValues.get(i) - j)) {
                    isStraight = false;
                    break;
                }
            }
            if (isStraight) {
                return true;
            }
        }
        return false;
    }

    /**
     * Prépare la liste de comparaison des cartes en fonction de leurs occurrences et priorités.
     *
     * @param valueCounts Les occurrences des valeurs de cartes.
     * @return Une liste des valeurs triées pour la comparaison.
     */
    private List<Integer> prepareComparisonList(Map<Integer, Integer> valueCounts) {
        List<Integer> comparisonList = new ArrayList<>();

        List<Integer> quads = new ArrayList<>();
        List<Integer> trips = new ArrayList<>();
        List<Integer> pairs = new ArrayList<>();
        List<Integer> singles = new ArrayList<>();

        for (Map.Entry<Integer, Integer> entry : valueCounts.entrySet()) {
            int value = entry.getKey();
            int count = entry.getValue();
            if (count == 4) {
                quads.add(value);
            } else if (count == 3) {
                trips.add(value);
            } else if (count == 2) {
                pairs.add(value);
            } else {
                singles.add(value);
            }
        }

        Collections.sort(quads, Collections.reverseOrder());
        Collections.sort(trips, Collections.reverseOrder());
        Collections.sort(pairs, Collections.reverseOrder());
        Collections.sort(singles, Collections.reverseOrder());

        comparisonList.addAll(quads);
        comparisonList.addAll(trips);
        comparisonList.addAll(pairs);
        comparisonList.addAll(singles);

        return comparisonList;
    }

    @Override
    public int compareTo(PokerHand other) {
        if (this.combination == null && other.combination == null) {
            return 0;
        } else if (this.combination == null) {
            return -1;
        } else if (other.combination == null) {
            return 1;
        }

        int comboComparison = Integer.compare(this.combination.getStrength(), other.combination.getStrength());
        if (comboComparison != 0) {
            return comboComparison;
        }

        for (int i = 0; i < this.cardValues.size(); i++) {
            if (i >= other.cardValues.size()) {
                return 1;
            }
            int valueComparison = Integer.compare(this.cardValues.get(i), other.cardValues.get(i));
            if (valueComparison != 0) {
                return valueComparison;
            }
        }

        return 0;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Hand: ");
        for (Card card : cards) {
            sb.append(card.toString()).append(", ");
        }
        sb.append("Combination: ").append(combination);
        return sb.toString();
    }
}
