package edu.info0502.common;

import java.io.Serializable;

/**
 * Représentation d'une carte de jeu avec un symbole et une valeur.
 * Implémente Comparable pour le tri et Serializable pour la transmission réseau.
 */
public class PlayingCard implements Comparable<PlayingCard>, Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * Enumération des symboles des cartes.
     */
    public enum Suit {
        HEARTS, DIAMONDS, CLUBS, SPADES
    }

    /**
     * Enumération des valeurs des cartes.
     */
    public enum Rank {
        TWO(2), THREE(3), FOUR(4), FIVE(5), SIX(6), SEVEN(7),
        EIGHT(8), NINE(9), TEN(10), JACK(11), QUEEN(12), KING(13), ACE(14);

        private final int value;

        /**
         * Constructeur pour Rank.
         *
         * @param value La valeur numérique associée à la carte.
         */
        Rank(int value) {
            this.value = value;
        }

        /**
         * Retourne la valeur numérique de la carte.
         *
         * @return La valeur numérique.
         */
        public int getValue() {
            return value;
        }

        /**
         * Retourne un Rank à partir d'une chaîne de caractères.
         *
         * @param rankString La chaîne représentant la valeur de la carte.
         * @return Le Rank correspondant.
         * @throws IllegalArgumentException Si la chaîne ne correspond à aucune valeur.
         */
        public static Rank fromString(String rankString) {
            switch (rankString) {
                case "2": return TWO;
                case "3": return THREE;
                case "4": return FOUR;
                case "5": return FIVE;
                case "6": return SIX;
                case "7": return SEVEN;
                case "8": return EIGHT;
                case "9": return NINE;
                case "10": return TEN;
                case "J": return JACK;
                case "Q": return QUEEN;
                case "K": return KING;
                case "A": return ACE;
                default: throw new IllegalArgumentException("Valeur inconnue : " + rankString);
            }
        }
    }

    private Suit suit;
    private Rank rank;

    /**
     * Constructeur par défaut (nécessaire pour des outils comme Gson).
     */
    public PlayingCard() {}

    /**
     * Constructeur à partir de chaînes de caractères.
     *
     * @param rankString La chaîne représentant la valeur de la carte.
     * @param suitString La chaîne représentant le symbole de la carte.
     */
    public PlayingCard(String rankString, String suitString) {
        this.rank = Rank.fromString(rankString);
        this.suit = Suit.valueOf(suitString.toUpperCase());
    }

    /**
     * Constructeur à partir des énumérations.
     *
     * @param suit Le symbole de la carte.
     * @param rank La valeur de la carte.
     */
    public PlayingCard(Suit suit, Rank rank) {
        this.suit = suit;
        this.rank = rank;
    }

    /**
     * Retourne le symbole de la carte.
     *
     * @return Le symbole.
     */
    public Suit getSuit() {
        return suit;
    }

    /**
     * Définit le symbole de la carte.
     *
     * @param suit Le symbole.
     */
    public void setSuit(Suit suit) {
        this.suit = suit;
    }

    /**
     * Retourne la valeur de la carte.
     *
     * @return La valeur.
     */
    public Rank getRank() {
        return rank;
    }

    /**
     * Définit la valeur de la carte.
     *
     * @param rank La valeur.
     */
    public void setRank(Rank rank) {
        this.rank = rank;
    }

    /**
     * Représente la carte sous forme de chaîne.
     *
     * @return Une chaîne décrivant la carte.
     */
    @Override
    public String toString() {
        return rank + " of " + suit;
    }

    /**
     * Compare cette carte à une autre pour déterminer leur ordre.
     *
     * @param other La carte à comparer.
     * @return Un entier négatif, zéro ou positif selon l'ordre relatif.
     */
    @Override
    public int compareTo(PlayingCard other) {
        return Integer.compare(this.rank.getValue(), other.rank.getValue());
    }

    /**
     * Vérifie l'égalité entre cette carte et un autre objet.
     *
     * @param obj L'objet à comparer.
     * @return true si les cartes sont identiques, false sinon.
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj instanceof PlayingCard) {
            PlayingCard other = (PlayingCard) obj;
            return this.rank == other.rank && this.suit == other.suit;
        }
        return false;
    }

    /**
     * Retourne le code de hachage de la carte.
     *
     * @return Le code de hachage.
     */
    @Override
    public int hashCode() {
        return rank.hashCode() + suit.hashCode() * 31;
    }
}
