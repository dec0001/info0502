package edu.info0502.server;

/**
 * Enumération représentant les différentes phases d'une partie de Texas Hold'em.
 */
public enum GameStage {
    PRE_FLOP,
    FLOP,
    TURN,
    RIVER,
    SHOWDOWN
}
