package edu.info0502.server;

import edu.info0502.common.*;
import com.google.gson.Gson;

import java.util.*;

/**
 * Classe représentant une table pour le jeu de poker Texas Hold'em.
 */
public class PokerTable {
    private String tableIdentifier;
    private List<Player> participants;
    private Deck cardDeck;
    private List<Card> sharedCards;
    private boolean isGameActive;

    private int totalPot;
    private int currentWager;
    private GameStage currentStage;

    private int dealerIndex;
    private int smallBlindIndex;
    private int bigBlindIndex;
    private int activePlayerIndex;

    private Set<String> actedPlayers;
    private boolean bettingRoundInProgress;

    private Map<String, Integer> playerBets; // playerId -> amount wagered

    private Gson jsonHandler;

    private GameEventObserver gameObserver;

    public PokerTable(String tableIdentifier) {
        this.tableIdentifier = tableIdentifier;
        this.participants = new ArrayList<>();
        this.cardDeck = new Deck();
        this.cardDeck.shuffle();
        this.sharedCards = new ArrayList<>();
        this.isGameActive = false;

        this.totalPot = 0;
        this.currentWager = 0;
        this.currentStage = GameStage.PRE_FLOP;

        this.dealerIndex = -1;
        this.smallBlindIndex = -1;
        this.bigBlindIndex = -1;
        this.activePlayerIndex = -1;

        this.actedPlayers = new HashSet<>();
        this.bettingRoundInProgress = false;

        this.playerBets = new HashMap<>();

        this.jsonHandler = new Gson();
    }

    public String getTableIdentifier() {
        return tableIdentifier;
    }

    public List<Player> getParticipants() {
        return participants;
    }

    public List<Card> getSharedCards() {
        return sharedCards;
    }

    public boolean isGameActive() {
        return isGameActive;
    }

    public GameStage getCurrentStage() {
        return currentStage;
    }

    /**
     * Définit un observateur pour les événements de la partie.
     *
     * @param observer Instance de GameEventObserver.
     */
    public void setGameObserver(GameEventObserver observer) {
        this.gameObserver = observer;
    }

    /**
     * Ajoute un joueur à la table.
     *
     * @param player Le joueur à ajouter.
     * @return true si l'ajout est réussi, false sinon.
     */
    public synchronized boolean addParticipant(Player player) {
        if (participants.size() >= 8) {
            return false; // La table est complète
        }
        participants.add(player);
        System.out.println("Le joueur '" + player.getName() + "' (ID: " + player.getId() + ") a rejoint la table " + tableIdentifier);
        return true;
    }

    /**
     * Recherche un joueur par son identifiant.
     *
     * @param playerId Identifiant du joueur.
     * @return Le joueur correspondant ou null s'il n'existe pas.
     */
    public Player findPlayerById(String playerId) {
        for (Player player : participants) {
            if (player.getId().equals(playerId)) {
                return player;
            }
        }
        return null;
    }

    /**
     * Lance la partie de poker.
     */
    public synchronized void initiateGame() {
        if (isGameActive) {
            System.out.println("La partie sur la table " + tableIdentifier + " est déjà en cours.");
            return;
        }

        if (participants.size() < 2) {
            System.out.println("Pas assez de joueurs pour commencer la partie sur la table " + tableIdentifier);
            return;
        }

        isGameActive = true;

        if (participants.size() == 2) {
            dealerIndex = 0;
            smallBlindIndex = dealerIndex;
            bigBlindIndex = (dealerIndex + 1) % participants.size();
        } else {
            dealerIndex = 0;
            smallBlindIndex = (dealerIndex + 1) % participants.size();
            bigBlindIndex = (dealerIndex + 2) % participants.size();
        }

        activePlayerIndex = (bigBlindIndex + 1) % participants.size();
        placeBlinds();
        dealHoleCards();
        startNewBettingRound();
        System.out.println("La partie a commencé sur la table " + tableIdentifier);
    }

    /**
     * Positionne les blinds pour les joueurs.
     */
    private void placeBlinds() {
        int smallBlind = 10;
        int bigBlind = 20;

        Player smallBlindPlayer = participants.get(smallBlindIndex);
        smallBlindPlayer.deductChips(smallBlind);
        totalPot += smallBlind;
        playerBets.put(smallBlindPlayer.getId(), smallBlind);
        System.out.println(smallBlindPlayer.getName() + " poste une small blind de " + smallBlind);

        Player bigBlindPlayer = participants.get(bigBlindIndex);
        bigBlindPlayer.deductChips(bigBlind);
        totalPot += bigBlind;
        playerBets.put(bigBlindPlayer.getId(), bigBlind);
        currentWager = bigBlind;
        System.out.println(bigBlindPlayer.getName() + " poste une big blind de " + bigBlind);
    }

    /**
     * Distribue les cartes fermées à chaque joueur.
     */
    private void dealHoleCards() {
        for (Player player : participants) {
            List<Card> holeCards = new ArrayList<>();
            holeCards.add(cardDeck.drawCard());
            holeCards.add(cardDeck.drawCard());
            PokerHand hand = new PokerHand(holeCards);
            player.setHand(hand);
            System.out.println(player.getName() + " reçoit ses cartes fermées: " + hand.getCards());
        }
    }

    /**
     * Démarre une nouvelle phase de mise.
     */
    private void startNewBettingRound() {
        actedPlayers.clear();
        bettingRoundInProgress = true;

        if (currentStage == GameStage.PRE_FLOP) {
            activePlayerIndex = (bigBlindIndex + 1) % participants.size();
        } else {
            if (participants.size() == 2) {
                activePlayerIndex = dealerIndex;
            } else {
                activePlayerIndex = (dealerIndex + 1) % participants.size();
            }
        }

        System.out.println("Nouvelle phase de mise. C'est au tour de " + participants.get(activePlayerIndex).getName());
    }

    /**
     * Détermine si la phase de mise est terminée.
     *
     * @return true si toutes les actions nécessaires sont terminées, false sinon.
     */
    private boolean isBettingRoundFinished() {
        for (Player player : participants) {
            if (!player.hasFolded()) {
                int playerWager = playerBets.getOrDefault(player.getId(), 0);
                if (playerWager < currentWager) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * Passe à l'étape suivante du jeu.
     */
    private void proceedToNextStage() {
        switch (currentStage) {
            case PRE_FLOP:
                dealFlop();
                currentStage = GameStage.FLOP;
                break;
            case FLOP:
                dealTurn();
                currentStage = GameStage.TURN;
                break;
            case TURN:
                dealRiver();
                currentStage = GameStage.RIVER;
                break;
            case RIVER:
                currentStage = GameStage.SHOWDOWN;
                determineWinner();
                break;
            case SHOWDOWN:
                endGame();
                return;
        }

        playerBets.clear();
        currentWager = 0;
        startNewBettingRound();
    }

    /**
     * Fin de la partie.
     */
    private void endGame() {
        System.out.println("La partie sur la table " + tableIdentifier + " est terminée.");
        isGameActive = false;
    }
}