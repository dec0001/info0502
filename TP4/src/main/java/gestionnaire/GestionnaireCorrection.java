import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rabbitmq.client.*;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GestionnaireCorrection {
    private static final String RESPONSES_QUEUE = "ReponsesQueue";
    private static final String RESULTS_QUEUE = "CorrectionsQueue";
    private static final String QCM_FILE = "qcm_correction.json";

    public static void main(String[] args) {
        try {
            ConnectionFactory factory = new ConnectionFactory();
            factory.setHost("localhost");

            try (Connection connection = factory.newConnection();
                 Channel channel = connection.createChannel()) {

                // Déclarer les files de messages
                channel.queueDeclare(RESPONSES_QUEUE, false, false, false, null);
                channel.queueDeclare(RESULTS_QUEUE, false, false, false, null);

                // Consommer les réponses soumises
                channel.basicConsume(RESPONSES_QUEUE, true, (consumerTag, delivery) -> {
                    String message = new String(delivery.getBody(), "UTF-8");
                    System.out.println("Réponses reçues : " + message);

                    // Charger les bonnes réponses depuis le fichier
                    Map<Integer, String> bonnesReponses = chargerBonnesReponses();

                    // Corriger les réponses
                    Map<String, Object> resultat = corrigerReponses(message, bonnesReponses);

                    // Envoyer le résultat à l'utilisateur
                    ObjectMapper mapper = new ObjectMapper();
                    String resultatJson = mapper.writeValueAsString(resultat);
                    channel.basicPublish("", RESULTS_QUEUE, null, resultatJson.getBytes());
                    System.out.println("Résultat envoyé : " + resultatJson);
                }, consumerTag -> {});
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static Map<Integer, String> chargerBonnesReponses() throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        File file = new File(QCM_FILE);
        Map<Integer, String> bonnesReponses = new HashMap<>();

        if (file.exists()) {
            Map<String, Object> qcmData = mapper.readValue(file, new TypeReference<>() {});
            List<Map<String, Object>> questions = (List<Map<String, Object>>) qcmData.get("questions");
            for (Map<String, Object> question : questions) {
                bonnesReponses.put((Integer) question.get("idQuestion"), (String) question.get("bonneReponse"));
            }
        }

        return bonnesReponses;
    }

    private static Map<String, Object> corrigerReponses(String message, Map<Integer, String> bonnesReponses) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        Map<String, Object> reponsesData = mapper.readValue(message, new TypeReference<>() {});
        List<Map<String, Object>> reponses = (List<Map<String, Object>>) reponsesData.get("reponses");

        int score = 0;
        Map<String, Object> resultat = new HashMap<>();
        Map<Integer, Boolean> details = new HashMap<>();

        for (Map<String, Object> reponse : reponses) {
            int idQuestion = (int) reponse.get("idQuestion");
            String reponseUtilisateur = (String) reponse.get("reponse");

            boolean correcte = bonnesReponses.get(idQuestion).equals(reponseUtilisateur);
            if (correcte) {
                score++;
            }
            details.put(idQuestion, correcte);
        }

        resultat.put("action", "resultatCorrection");
        resultat.put("idUtilisateur", reponsesData.get("idUtilisateur"));
        resultat.put("idQCM", reponsesData.get("idQCM"));
        resultat.put("score", score);
        resultat.put("details", details);

        return resultat;
    }
}
