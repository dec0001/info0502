import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

public class GestionnaireQCM {
    private static final String REQUEST_QUEUE = "DemandeQCMQueue";
    private static final String RESPONSE_QUEUE = "ReponsesQCMQueue";

    public static void main(String[] args) {
        try {
            ConnectionFactory factory = new ConnectionFactory();
            factory.setHost("localhost");

            try (Connection connection = factory.newConnection();
                 Channel channel = connection.createChannel()) {

                // Déclarer les files de messages
                channel.queueDeclare(REQUEST_QUEUE, false, false, false, null);
                channel.queueDeclare(RESPONSE_QUEUE, false, false, false, null);

                // Consommer les demandes de QCM
                channel.basicConsume(REQUEST_QUEUE, true, (consumerTag, delivery) -> {
                    String message = new String(delivery.getBody(), "UTF-8");
                    System.out.println("Demande reçue : " + message);

                    // Simuler un QCM
                    String qcm = """
                        {
                            "idQCM": "QCM12345",
                            "titre": "Introduction à la Programmation",
                            "questions": [
                                {"idQuestion": 1, "texte": "Quelle est la syntaxe correcte pour afficher un message en Java ?", "options": ["System.out.println()", "Console.WriteLine()", "print()", "echo()"]},
                                {"idQuestion": 2, "texte": "Quelle est la portée par défaut d'une variable déclarée dans une classe en Java ?", "options": ["Public", "Private", "Protected", "Package-private"]}
                            ]
                        }
                        """;

                    // Envoyer le QCM à l'utilisateur
                    channel.basicPublish("", RESPONSE_QUEUE, null, qcm.getBytes());
                    System.out.println("QCM envoyé.");
                }, consumerTag -> {});
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
