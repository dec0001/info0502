public class SoumissionReponses {
    private static final String ANSWERS_QUEUE = "ReponsesQueue";

    public static void main(String[] args) {
        try {
            ConnectionFactory factory = new ConnectionFactory();
            factory.setHost("localhost");

            try (Connection connection = factory.newConnection();
                 Channel channel = connection.createChannel()) {

                // Soumission des réponses
                String reponses = """
                    {
                        "action": "soumissionReponses",
                        "idUtilisateur": "U123",
                        "idQCM": "QCM12345",
                        "reponses": [
                            {"idQuestion": 1, "reponse": "System.out.println()"},
                            {"idQuestion": 2, "reponse": "Package-private"}
                        ]
                    }
                    """;
                channel.queueDeclare(ANSWERS_QUEUE, false, false, false, null);
                channel.basicPublish("", ANSWERS_QUEUE, null, reponses.getBytes());
                System.out.println("Réponses soumises : " + reponses);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
