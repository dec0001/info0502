import com.rabbitmq.client.*;

public class ClientQCM {
    private static final String REQUEST_QUEUE = "DemandeQCMQueue";
    private static final String RESPONSE_QUEUE = "ReponsesQCMQueue";

    public static void main(String[] args) {
        try {
            ConnectionFactory factory = new ConnectionFactory();
            factory.setHost("localhost");

            try (Connection connection = factory.newConnection();
                 Channel channel = connection.createChannel()) {

                // Envoyer une demande de QCM
                String demandeQCM = """
                    {
                        "action": "demandeQCM",
                        "idUtilisateur": "U123",
                        "categorie": "Programmation"
                    }
                    """;
                channel.queueDeclare(REQUEST_QUEUE, false, false, false, null);
                channel.basicPublish("", REQUEST_QUEUE, null, demandeQCM.getBytes());
                System.out.println("Demande envoyée : " + demandeQCM);

                // Consommer la réponse contenant le QCM
                channel.queueDeclare(RESPONSE_QUEUE, false, false, false, null);
                channel.basicConsume(RESPONSE_QUEUE, true, (consumerTag, delivery) -> {
                    String qcm = new String(delivery.getBody(), "UTF-8");
                    System.out.println("QCM reçu : " + qcm);
                }, consumerTag -> {});
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
