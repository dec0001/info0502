import java.util.HashMap;
import java.util.Map;

public class Utilisateur {
    private String idUtilisateur;
    private String nom;
    private String email;
    private Map<String, Integer> scores; // QCM ID -> Score

    public Utilisateur(String idUtilisateur, String nom, String email) {
        this.idUtilisateur = idUtilisateur;
        this.nom = nom;
        this.email = email;
        this.scores = new HashMap<>();
    }

    public String getIdUtilisateur() {
        return idUtilisateur;
    }

    public String getNom() {
        return nom;
    }

    public String getEmail() {
        return email;
    }

    public Map<String, Integer> getScores() {
        return scores;
    }

    public void addScore(String idQCM, int score) {
        scores.put(idQCM, score);
    }
}
