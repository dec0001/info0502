import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class GestionnaireUtilisateur {
    private List<Utilisateur> utilisateurs;
    private static final String FILE_PATH = "utilisateurs.json";

    public GestionnaireUtilisateur() {
        utilisateurs = new ArrayList<>();
        chargerUtilisateursDepuisFichier();
    }

    public void ajouterUtilisateur(Utilisateur utilisateur) {
        utilisateurs.add(utilisateur);
        sauvegarderUtilisateursDansFichier();
    }

    public void supprimerUtilisateur(String idUtilisateur) {
        utilisateurs.removeIf(u -> u.getIdUtilisateur().equals(idUtilisateur));
        sauvegarderUtilisateursDansFichier();
    }

    public Utilisateur getUtilisateur(String idUtilisateur) {
        return utilisateurs.stream()
                .filter(u -> u.getIdUtilisateur().equals(idUtilisateur))
                .findFirst()
                .orElse(null);
    }

    public void ajouterScore(String idUtilisateur, String idQCM, int score) {
        Utilisateur utilisateur = getUtilisateur(idUtilisateur);
        if (utilisateur != null) {
            utilisateur.addScore(idQCM, score);
            sauvegarderUtilisateursDansFichier();
        }
    }

    private void chargerUtilisateursDepuisFichier() {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            File file = new File(FILE_PATH);
            if (file.exists()) {
                utilisateurs = objectMapper.readValue(file, new TypeReference<List<Utilisateur>>() {});
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void sauvegarderUtilisateursDansFichier() {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            objectMapper.writeValue(new File(FILE_PATH), utilisateurs);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
