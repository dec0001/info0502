public class Main {
    public static void main(String[] args) {
        GestionnaireUtilisateur gestionnaire = new GestionnaireUtilisateur();

        // Ajouter un utilisateur
        Utilisateur utilisateur = new Utilisateur("U123", "Jean Dupont", "jean.dupont@example.com");
        gestionnaire.ajouterUtilisateur(utilisateur);

        // Ajouter un score
        gestionnaire.ajouterScore("U123", "QCM12345", 90);

        // Récupérer un utilisateur
        Utilisateur u = gestionnaire.getUtilisateur("U123");
        if (u != null) {
            System.out.println("Utilisateur trouvé : " + u.getNom());
        }

        // Supprimer un utilisateur
        gestionnaire.supprimerUtilisateur("U123");
    }
}
