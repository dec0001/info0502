package client;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PokerClientTest {
    private Thread mockServerThread;
    private ServerSocket mockServer;

    @BeforeEach
    void setUp() {
        mockServerThread = new Thread(() -> {
            try {
                mockServer = new ServerSocket(12345);
                Socket clientSocket = mockServer.accept();
                ObjectOutputStream out = new ObjectOutputStream(clientSocket.getOutputStream());
                ObjectInputStream in = new ObjectInputStream(clientSocket.getInputStream());

                // Lire le pseudonyme envoyé par le client
                String pseudo = (String) in.readObject();
                out.writeObject(pseudo + " a rejoint la partie.");
                out.flush();

                clientSocket.close();
            } catch (IOException | ClassNotFoundException e) {
                e.printStackTrace();
            }
        });
        mockServerThread.start();

        // Attendre que le serveur factice démarre
        try {
            Thread.sleep(1000); // Donne le temps au serveur de démarrer
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @AfterEach
    void tearDown() {
        try {
            mockServer.close();
            mockServerThread.interrupt();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    void testClientInteraction() throws IOException {
        // Simule un client se connectant au serveur factice
        ByteArrayInputStream input = new ByteArrayInputStream("Player1\nQUIT\n".getBytes());
        System.setIn(input);
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        System.setOut(new PrintStream(output));

        PokerClient.main(new String[]{});

        // Vérifie que le client affiche les messages correctement
        String logs = output.toString();
        assertEquals(true, logs.contains("Player1 a rejoint la partie."));
    }
}
