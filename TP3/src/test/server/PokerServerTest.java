package server;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.*;
import java.net.Socket;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PokerServerTest {
    private Thread serverThread;

    @BeforeEach
    void setUp() {
        // Lancer le serveur dans un thread séparé
        serverThread = new Thread(() -> PokerServer.main(new String[]{}));
        serverThread.start();

        // Attendre que le serveur démarre
        try {
            Thread.sleep(1000); // Donne le temps au serveur de démarrer
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @AfterEach
    void tearDown() {
        serverThread.interrupt(); // Arrête le serveur
    }

    @Test
    void testClientConnection() throws IOException, ClassNotFoundException {
        // Simule une connexion client
        Socket clientSocket = new Socket("localhost", 12345);
        ObjectOutputStream out = new ObjectOutputStream(clientSocket.getOutputStream());
        ObjectInputStream in = new ObjectInputStream(clientSocket.getInputStream());

        // Envoyer un pseudonyme au serveur
        String pseudo = "Player1";
        out.writeObject(pseudo);
        out.flush();

        // Vérifier que le serveur répond correctement
        Object response = in.readObject();
        assertEquals(pseudo + " a rejoint la partie.", response);

        clientSocket.close();
    }
}
