package server;

import java.io.*;
import java.net.*;
import java.util.*;

public class PokerServer {
    private static final int PORT = 12345;
    private List<PlayerHandler> players = new ArrayList<>();
    private Deck deck = new Deck();

    public static void main(String[] args) {
        new PokerServer().startServer();
    }

    public void startServer() {
        try (ServerSocket serverSocket = new ServerSocket(PORT)) {
            System.out.println("Serveur démarré sur le port " + PORT);

            while (true) {
                Socket clientSocket = serverSocket.accept();
                PlayerHandler playerHandler = new PlayerHandler(clientSocket);
                players.add(playerHandler);
                new Thread(playerHandler).start();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private class PlayerHandler implements Runnable {
        private Socket socket;
        private ObjectOutputStream out;
        private ObjectInputStream in;
        private String username;

        public PlayerHandler(Socket socket) {
            this.socket = socket;
        }

        @Override
        public void run() {
            try {
                out = new ObjectOutputStream(socket.getOutputStream());
                in = new ObjectInputStream(socket.getInputStream());

                // Étape 1 : Récupération du pseudonyme
                username = (String) in.readObject();
                System.out.println(username + " s'est connecté.");
                sendToAll(username + " a rejoint la partie.");

                // Étape 2 : Attente et gestion de la partie
                if (players.size() >= 2) {
                    startGame();
                }

            } catch (IOException | ClassNotFoundException e) {
                e.printStackTrace();
            } finally {
                closeConnection();
            }
        }

        private void sendToAll(String message) throws IOException {
            for (PlayerHandler player : players) {
                player.out.writeObject(message);
                player.out.flush();
            }
        }

        private void closeConnection() {
            try {
                players.remove(this);
                socket.close();
                System.out.println(username + " s'est déconnecté.");
                sendToAll(username + " a quitté la partie.");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void startGame() throws IOException {
        System.out.println("Début de la partie!");
        deck.shuffle();

        
        for (PlayerHandler player : players) {
            List<Card> privateCards = deck.deal(2);
            player.out.writeObject("Vos cartes privées : " + privateCards);
            player.out.flush();
        }

        
        List<Card> communityCards = deck.deal(5);
        for (PlayerHandler player : players) {
            player.out.writeObject("Cartes communes : " + communityCards);
            player.out.flush();
        }

        
        sendToAll("La partie est terminée. Merci d'avoir joué !");
    }
}
