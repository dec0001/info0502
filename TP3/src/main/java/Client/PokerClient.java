package client;

import java.io.*;
import java.net.*;
import java.util.Scanner;

/**
 * Classe principale du client de poker.
 * Permet au joueur de se connecter au serveur et de suivre la partie.
 */
public class PokerClient {
    private static final String SERVER_ADDRESS = "localhost"; // Adresse du serveur
    private static final int SERVER_PORT = 12345; // Port du serveur

    public static void main(String[] args) {
        new PokerClient().startClient();
    }

    public void startClient() {
        try (Socket socket = new Socket(SERVER_ADDRESS, SERVER_PORT);
             ObjectOutputStream out = new ObjectOutputStream(socket.getOutputStream());
             ObjectInputStream in = new ObjectInputStream(socket.getInputStream());
             Scanner scanner = new Scanner(System.in)) {

            System.out.println("Connecté au serveur : " + SERVER_ADDRESS + ":" + SERVER_PORT);

            // Étape 1 : Envoi du pseudonyme
            System.out.print("Entrez votre pseudonyme : ");
            String username = scanner.nextLine();
            out.writeObject(username);
            out.flush();

            // Étape 2 : Suivi des messages du serveur
            Thread listenThread = new Thread(() -> {
                try {
                    Object message;
                    while ((message = in.readObject()) != null) {
                        System.out.println("Message du serveur : " + message);
                    }
                } catch (IOException | ClassNotFoundException e) {
                    System.out.println("Connexion au serveur perdue.");
                }
            });
            listenThread.start();

            // Étape 3 : Gestion des commandes du client
            String command;
            while (true) {
                System.out.print("Entrez une commande (QUIT pour quitter) : ");
                command = scanner.nextLine();

                if ("QUIT".equalsIgnoreCase(command)) {
                    out.writeObject("QUIT");
                    out.flush();
                    break;
                } else {
                    out.writeObject(command);
                    out.flush();
                }
            }

        } catch (IOException e) {
            System.out.println("Impossible de se connecter au serveur : " + e.getMessage());
        }
    }
}