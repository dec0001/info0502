package client;

import java.io.Serializable;

public class Card implements Serializable {
    private String suit;
    private String rank;

    public Card(String suit, String rank) {
        this.suit = suit;
        this.rank = rank;
    }

    @Override
    public String toString() {
        return rank + " de " + suit;
    }
}
