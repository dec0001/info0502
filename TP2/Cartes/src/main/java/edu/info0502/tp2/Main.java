package edu.info0502.tp2;


import java.util.ArrayList;
import java.util.List;
import java.util.Arrays;

/**
 * Classe représentant une main de poker avec 5 cartes tirées du talon.
 * Question TP2 : Définit une main et les méthodes spécifiques pour évaluer et comparer les combinaisons.
 */
public class Main {
    private ArrayList<Carte> cartes; // Liste des cartes dans la main

    /**
     * Constructeur de la classe Main : crée une main avec 5 cartes tirées du talon
     * Question TP2 : Tire 5 cartes pour créer une main.
     * @param talon Le talon duquel les cartes sont tirées
     */
    public Main(Talon talon) {
        cartes = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            cartes.add(talon.tirerCarte());
        }
    }

    /**
     * Affiche toutes les cartes de la main
     * Question TP2 : Méthode utilisée pour vérifier la distribution de la main.
     */
    public void afficherMain() {
        for (Carte carte : cartes) {
            System.out.println(carte);
        }
    }

    /**
     * Méthode pour évaluer la main (détermine la carte la plus forte)
     * Question TP2 : Évalue la carte la plus haute dans la main.
     * @return La carte avec la plus haute valeur
     */
    public Carte carteLaPlusHaute() {
        Carte carteMax = cartes.get(0);
        for (Carte carte : cartes) {
            // Comparaison des valeurs de cartes pour trouver la carte la plus forte
            if (comparerValeurs(carte, carteMax) > 0) {
                carteMax = carte;
            }
        }
        return carteMax;
    }

    /**
     * Méthode privée pour comparer les valeurs des cartes
     * @param carte1 La première carte
     * @param carte2 La deuxième carte
     * @return Un entier indiquant la comparaison (1, -1, ou 0)
     */
    private int comparerValeurs(Carte carte1, Carte carte2) {
        String[] valeurs = {"2", "3", "4", "5", "6", "7", "8", "9", "10", "Valet", "Dame", "Roi", "As"};
        List<String> listeValeurs = Arrays.asList(valeurs);
        return Integer.compare(listeValeurs.indexOf(carte1.getValeur()), listeValeurs.indexOf(carte2.getValeur()));
    }

    /**
     * Méthode statique pour comparer deux mains
     * Question TP2 : Permet de comparer deux mains pour déterminer la plus forte.
     * @param main1 La première main
     * @param main2 La deuxième main
     * @return Un entier indiquant la comparaison (1 si main1 est plus forte, -1 si main2 est plus forte, 0 si égales)
     */
    public static int comparerDeuxMains(Main main1, Main main2) {
        Carte carteMaxMain1 = main1.carteLaPlusHaute();
        Carte carteMaxMain2 = main2.carteLaPlusHaute();
        return main1.comparerValeurs(carteMaxMain1, carteMaxMain2);
    }
}
