package edu.info0502.tp2;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Classe représentant un paquet de 52 cartes de poker.
 * Question TP2 : Définit un ensemble de cartes (Deck) et les méthodes spécifiques pour le manipuler.
 */
public class Deck {
    private ArrayList<Carte> cartes; // Liste des cartes du paquet

    /**
     * Constructeur de la classe Deck : crée un paquet standard de 52 cartes
     * Question TP2 : Génère un ensemble de cartes en associant chaque valeur à chaque couleur.
     */
    public Deck() {
        cartes = new ArrayList<>();
        String[] valeurs = {"2", "3", "4", "5", "6", "7", "8", "9", "10", "Valet", "Dame", "Roi", "As"};
        String[] couleurs = {"Trèfle", "Carreau", "Cœur", "Pique"};

        for (String couleur : couleurs) {
            for (String valeur : valeurs) {
                cartes.add(new Carte(valeur, couleur));
            }
        }
    }

    /**
     * Mélanger le paquet de cartes
     * Question TP2 : Méthode spécifique pour mélanger un ensemble de cartes.
     */
    public void melanger() {
        Collections.shuffle(cartes);
    }

    /**
     * Tirer une carte du paquet
     * @return La carte tirée, ou null si le paquet est vide
     * Question TP2 : Méthode spécifique permettant de tirer une carte.
     */
    public Carte tirerCarte() {
        if (!cartes.isEmpty()) {
            return cartes.remove(0);
        } else {
            return null;  // Plus de cartes à tirer
        }
    }

    /**
     * Affiche toutes les cartes restantes dans le paquet
     * Question TP2 : Méthode utilisée pour les phases de test, montrant l'état du paquet.
     */
    public void afficherCartes() {
        for (Carte carte : cartes) {
            System.out.println(carte);
        }
    }
}
