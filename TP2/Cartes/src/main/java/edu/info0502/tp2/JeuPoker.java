package edu.info0502.tp2;


import java.util.ArrayList;
public class JeuPoker {
    public static void main(String[] args) {
        Talon talon = new Talon(1);  // Un talon avec un paquet de cartes
        talon.melanger();  // Mélanger les cartes

        Main main1 = new Main(talon);
        Main main2 = new Main(talon);

        System.out.println("Main 1 :");
        main1.afficherMain();
        System.out.println("Carte la plus haute de la Main 1 : " + main1.carteLaPlusHaute());

        System.out.println("\nMain 2 :");
        main2.afficherMain();
        System.out.println("Carte la plus haute de la Main 2 : " + main2.carteLaPlusHaute());

        // Comparer les deux mains
        int comparaison = Main.comparerDeuxMains(main1, main2);
        if (comparaison > 0) {
            System.out.println("\nMain 1 est plus forte.");
        } else if (comparaison < 0) {
            System.out.println("\nMain 2 est plus forte.");
        } else {
            System.out.println("\nLes deux mains sont égales.");
        }
    }
}
package edu.info0502.tp1;

import java.util.ArrayList;

/**
 * Classe principale pour tester les fonctionnalités de l'application de poker.
 * Question TP2 : Programme de démonstration pour les méthodes spécifiques : mélange, tirage, évaluation et comparaison de mains.
 */
public class JeuPoker {
    public static void main(String[] args) {
        // Initialisation du talon (un ensemble de paquets)
        Talon talon = new Talon(1);  // Un talon avec un paquet de cartes
        talon.melanger();  // Mélanger les cartes

        // Création de deux mains de 5 cartes chacune, tirées du talon
        Main main1 = new Main(talon);
        Main main2 = new Main(talon);

        // Affichage de la première main
        System.out.println("Main 1 :");
        main1.afficherMain();
        System.out.println("Carte la plus haute de la Main 1 : " + main1.carteLaPlusHaute());

        // Affichage de la deuxième main
        System.out.println("\nMain 2 :");
        main2.afficherMain();
        System.out.println("Carte la plus haute de la Main 2 : " + main2.carteLaPlusHaute());

        // Comparaison des deux mains pour déterminer laquelle est la plus forte
        int comparaison = Main.comparerDeuxMains(main1, main2);
        if (comparaison > 0) {
            System.out.println("\nMain 1 est plus forte.");
        } else if (comparaison < 0) {
            System.out.println("\nMain 2 est plus forte.");
        } else {
            System.out.println("\nLes deux mains sont égales.");
        }
    }
}
