package edu.info0502.tp2;

/**
 * Classe représentant une carte de poker avec une valeur et une couleur.
 * Question TP2 : Définit un élément de base du jeu, la carte.
 */
public class Carte {
    private String valeur; // Valeur de la carte (2, 3, ..., Roi, As)
    private String couleur; // Couleur de la carte (Trèfle, Carreau, Coeur, Pique)

    /**
     * Constructeur de la classe Carte
     * Question TP2 : Permet de créer une carte avec une valeur et une couleur données.
     * @param valeur La valeur de la carte
     * @param couleur La couleur de la carte
     */
    public Carte(String valeur, String couleur) {
        this.valeur = valeur;
        this.couleur = couleur;
    }

    // Getters

    /**
     * Retourne la valeur de la carte
     * @return La valeur de la carte
     */
    public String getValeur() {
        return valeur;
    }

    /**
     * Retourne la couleur de la carte
     * @return La couleur de la carte
     */
    public String getCouleur() {
        return couleur;
    }

    // Setters

    /**
     * Définit la valeur de la carte
     * @param valeur La nouvelle valeur de la carte
     */
    public void setValeur(String valeur) {
        this.valeur = valeur;
    }

    /**
     * Définit la couleur de la carte
     * @param couleur La nouvelle couleur de la carte
     */
    public void setCouleur(String couleur) {
        this.couleur = couleur;
    }

    /**
     * Affiche la carte sous forme de chaîne de caractères
     * @return Une représentation de la carte sous la forme "valeur de couleur"
     */
    @Override
    public String toString() {
        return valeur + " de " + couleur;
    }
}
