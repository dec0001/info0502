package edu.info0502.tp2;


import java.util.ArrayList;

public class TestJeuPoker {
    public static void main(String[] args) {
        // 1. Mise en place d'un talon
        System.out.println("Mise en place d'un talon de cartes :");
        Talon talon = new Talon(1);  // Un talon avec un paquet de cartes
        talon.melanger();
        talon.afficherCartes();

        // 2. Création et évaluation des mains
        System.out.println("\nCréation et évaluation des mains :");
        Main main1 = new Main(talon);
        Main main2 = new Main(talon);

        System.out.println("Main 1 :");
        main1.afficherMain();
        System.out.println("Carte la plus haute de la Main 1 : " + main1.carteLaPlusHaute());

        System.out.println("\nMain 2 :");
        main2.afficherMain();
        System.out.println("Carte la plus haute de la Main 2 : " + main2.carteLaPlusHaute());

        // 3. Simulation d'une partie de poker simple avec 4 joueurs
        System.out.println("\nSimulation d'une partie de poker simple avec 4 joueurs :");
        ArrayList<Main> mainsJoueurs = new ArrayList<>();
        
        // Distribuer une main à chaque joueur (5 cartes chacun)
        for (int i = 0; i < 4; i++) {
            Main mainJoueur = new Main(talon);
            mainsJoueurs.add(mainJoueur);
            System.out.println("\nMain du Joueur " + (i + 1) + " :");
            mainJoueur.afficherMain();
        }

        // Déterminer la main gagnante en se basant sur la carte la plus haute
        Main mainGagnante = mainsJoueurs.get(0);
        int indexGagnant = 0;
        for (int i = 1; i < mainsJoueurs.size(); i++) {
            if (Main.comparerDeuxMains(mainGagnante, mainsJoueurs.get(i)) < 0) {
                mainGagnante = mainsJoueurs.get(i);
                indexGagnant = i;
            }
        }

        System.out.println("\nLa main gagnante est celle du Joueur " + (indexGagnant + 1) + " :");
        mainGagnante.afficherMain();
    }
}
