package edu.info0502.tp2;


import java.util.ArrayList;
import java.util.Collections;

/**
 * Classe représentant un talon de cartes (un ou plusieurs paquets mélangés).
 * Question TP2 : Définit le talon, composé de plusieurs decks, et les méthodes pour tirer et mélanger.
 */
public class Talon {
    private ArrayList<Carte> cartes; // Liste de toutes les cartes dans le talon

    /**
     * Constructeur de la classe Talon : crée un talon à partir de plusieurs paquets mélangés
     * Question TP2 : Permet de combiner plusieurs decks pour former un talon.
     * @param nombrePaquets Le nombre de paquets dans le talon
     */
    public Talon(int nombrePaquets) {
        cartes = new ArrayList<>();
        for (int i = 0; i < nombrePaquets; i++) {
            Deck deck = new Deck();
            deck.melanger();
            cartes.addAll(deck.getCartes());
        }
        Collections.shuffle(cartes);  // Mélange final du talon complet
    }

    /**
     * Tirer une carte du talon
     * @return La carte tirée, ou null si le talon est vide
     * Question TP2 : Méthode spécifique permettant de tirer une carte du talon.
     */
    public Carte tirerCarte() {
        if (!cartes.isEmpty()) {
            return cartes.remove(0);
        } else {
            return null;  // Plus de cartes à tirer
        }
    }

    /**
     * Affiche toutes les cartes restantes dans le talon
     * Question TP2 : Méthode utilisée pour tester l'état du talon, affichant son contenu.
     */
    public void afficherCartes() {
        for (Carte carte : cartes) {
            System.out.println(carte);
        }
    }

    /**
     * Mélange les cartes dans le talon
     * Question TP2 : Méthode spécifique pour mélanger un ensemble de cartes.
     */
    public void melanger() {
        Collections.shuffle(cartes);
    }
}
