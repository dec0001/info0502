package edu.info0502.tp1;

import java.util.Vector;

public class Mediatheque {
    // Données d'instance : nom du propriétaire et collection de médias
    private String proprietaire;
    private Vector<Media> medias;

    // Méthode main (point d'entrée)
    public static void main(String[] args) {
        System.out.println("Bienvenue dans l'application Médiathèque !");
        // Vous pouvez ici instancier une Mediatheque pour tester les méthodes
        Mediatheque mediatheque = new Mediatheque("Jean Dupont");
        System.out.println(mediatheque.toString());
    }

    // Constructeur par défaut (Question 10)
    public Mediatheque() {
        this.proprietaire = "";
        this.medias = new Vector<>();
    }

    // Constructeur par initialisation (Question 10)
    public Mediatheque(String proprietaire) {
        this.proprietaire = proprietaire;
        this.medias = new Vector<>();
    }

    // Constructeur par copie (Question 10)
    public Mediatheque(Mediatheque autreMediatheque) {
        this.proprietaire = autreMediatheque.proprietaire;
        this.medias = new Vector<>(autreMediatheque.medias);  // Copie de la collection de médias
    }

    // Méthode pour ajouter un média à la médiathèque (Question 10)
    public void add(Media media) {
        medias.add(media);
    }

    // Méthode pour afficher les informations sur la médiathèque (Question 10)
    @Override
    public String toString() {
        StringBuilder result = new StringBuilder("Mediatheque du proprietaire " + proprietaire + " : \n");
        for (Media media : medias) {
            result.append(media.toString()).append("\n");  
        }
        return result.toString();
    }

    // Getter pour obtenir la liste des médias (facultatif, pour manipuler les médias depuis l'extérieur)
    public Vector<Media> getMedias() {
        return medias;
    }
}
