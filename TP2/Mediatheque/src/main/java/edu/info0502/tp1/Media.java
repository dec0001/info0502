package edu.info0502.tp1;

public class Media {
    // Données d'instance privées
    private String titre;         
    private StringBuffer cote;    
    private int note;             

    // Donnée de classe privée
    private static String nomMediatheque = "Mediatheque Centrale"; // Question 2

    // Constructeur par défaut (Question 2)
    public Media() {
        this.titre = "";
        this.cote = new StringBuffer();
        this.note = 0;
    }

    // Constructeur par initialisation (Question 2)
    public Media(String titre, StringBuffer cote, int note) {
        this.titre = titre;
        this.cote = cote;
        this.note = note;
    }

    // Constructeur par copie (Question 2)
    public Media(Media autreMedia) {
        this.titre = autreMedia.titre;
        this.cote = new StringBuffer(autreMedia.cote);
        this.note = autreMedia.note;
    }

    // Getters et Setters pour les données d'instance (Question 2)
    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    // Getter pour cote (modifié pour répondre à la Question 3)
    public StringBuffer getCote() {
        return new StringBuffer(cote);  // Retourne une copie pour éviter les modifications non désirées
    }

    public void setCote(StringBuffer cote) {
        this.cote = cote;
    }

    public int getNote() {
        return note;
    }

    public void setNote(int note) {
        this.note = note;
    }

    // Méthodes de classe pour accéder à nomMediatheque (Question 2)
    public static String getNomMediatheque() {
        return nomMediatheque;
    }

    public static void setNomMediatheque(String nom) {
        nomMediatheque = nom;
    }

    // Masquage des méthodes clone, equals, et toString (Question 2)
    @Override
    public String toString() {
        return "Media{" +
               "titre='" + titre + '\'' +
               ", cote=" + cote +
               ", note=" + note +
               '}';
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        Media media = (Media) obj;
        return note == media.note &&
               titre.equals(media.titre) &&
               cote.toString().equals(media.cote.toString());
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return new Media(this);  // Retourne un nouvel objet en utilisant le constructeur par copie
    }
}
